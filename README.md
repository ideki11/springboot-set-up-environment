# SpringBoot Resep Masakan - Set Up Environment



## Getting started
Berikut merupakan guide atau langkah-langkah untuk Setting Environment yang butuhkan.

## Prerequisite
- Java jdk 17.\
Link Download : https://www.oracle.com/id/java/technologies/downloads/#jdk17-windows
- Maven 3.6 keatas.\
Link Download : https://maven.apache.org/download.cgi
- PostgreSQL 12 Keatas.\
Link Download : https://www.enterprisedb.com/downloads/postgres-postgresql-downloads
- IDE, IntelliJ, VsCode atau Eclipse.\
Link Download :\
VScode : https://code.visualstudio.com/Download \
Eclipse : https://www.eclipse.org/downloads/packages/release/2024-03/r \
IntelliJ IDE (Jet Brains IDE) : https://www.jetbrains.com/idea/download/?section=windows (Free 30 day)
- Pgadmin atau dbeaver.\
Link Download dbeaver : https://dbeaver.io/download/
- Postman / Thunder Client (Vs Code Extenstions).\
Link Download:\
Postman : https://www.postman.com/downloads/?utm_source=postman-home

## YouTube Overview

[![SettingEnvironmentYT](https://img.youtube.com/vi/mttsAL7oR2U/0.jpg)](https://www.youtube.com/watch?v=mttsAL7oR2U)

## Step by step

> Guide ini akan dimulai dari download dan setting environment yang dibutuhkan untuk nanti membuat Project Latihan Resep Masakan.

> 1.  Setting Environment Java jdk 17
- Download Java jdk 17, buka link https://www.oracle.com/id/java/technologies/downloads/#jdk17-windows. Kemudian pilih tab java 17, setelah itu pilih Tab Windows (Sesuaikan dengan OS yang digunakan), setelah itu pilih x64 Installer dengan extenstion file .exe. Setelah itu tunggu sampai downlad file selesai.
- Install Java jdk 17\
    ![Java Install][def]

[def]: img/JavaInstallation.PNG

- Setting Environment Variable pada windows. Tekan tombol windows pada keyboard kemudian ketik "Edit the system environment Varibles". Kemudian pilih System Variable.\
![System Properties][def2]

[def2]: img/SystemPropertiesWindwos.PNG

- Pada Users Variables pilih New, Kemudian pada **variable name** tuliskan "JAVA_HOME". Pilih Browse Directory, Kemudian cari folder atau directory tempat dimana Java di install, Defaultnya terinstall di "C:\Program Files\Java\jdk-17". Atau bisa juga tuliskan Secara langsung di **varialbe value**.\
![UserVariables Java][def3]

[def3]: img/CreateNewUserVariablesJavaPadaWindows.PNG

- Pada System Variable pilih "path", kemudian pilih Edit.\
![UserVariables Java][def4] <br>

- Setelah itu Pilih New dan kemudian tambahkan Path tempat direktori bin yang ada di direktori java yang sudah terinstall. Defaultnya ada pada "C:\Program Files\Java\jdk-17\bin". Klik OK.\
![UserVariables Java][def6]

[def4]: img/EditPathPadaSystemVariable.PNG
[def6]: img/CreateNewPathJava.PNG

- Jika sudah selesai setting environment variablenya, kita bisa cek dengan cara buka command prompt atau cmd. Kemudian ketikan "java -version". Jika Setting sudah benar maka akan muncul informasi Java yang digunakan.
![UserVariables Java][def5]

[def5]: img/CMDJavaVersion.PNG


> 2. Setting Environment Maven
- Download Maven 3.9.6, buka link https://maven.apache.org/download.cgi. Kemudian pada Bagian File, pilih **"Binary zip archive"**.
- Setelah selesai download, Unzip Maven yang sudah didownload pada **drive C:**.

- Setting Environment Variable pada windows. Sama seperti pada java sebelumnya.\
![System Properties][def2]

- Pada Users Variables pilih New, Kemudian pada **variable name** tuliskan "MAVEN_HOME". Pilih Browse Directory, Kemudian cari folder atau directory tempat dimana Maven di Unzip, Yaitu ada di "C:\apache-maven-3.9.6". Atau bisa juga tuliskan Secara langsung di **varialbe value**.\
![Maven][def18]

- Pada System Variable pilih "path", kemudian pilih Edit. Setelah itu Pilih New dan kemudian tambahkan Path tempat direktori bin yang ada di direktori maven. Yaitu ada pada "C:\apache-maven-3.9.6\bin". Klik OK.\
![Maven][def19]

- Jika sudah selesai setting environment variablenya, kita bisa cek dengan cara buka command prompt atau cmd. Kemudian ketikan "java -version". Jika Setting sudah benar maka akan muncul informasi Java yang digunakan.\
![Maven][def20]

[def17]: img/MavenDownload.png
[def18]: img/CreateNewUserVariablesMavenPadaWindows.PNG
[def19]: img/EditPathMavenPadaSystemVariable.PNG
[def20]: img/CMDMavenVersion.PNG

> 3. Install PostgreSQL
- Download PostgreSQL 12, buka link https://www.enterprisedb.com/downloads/postgres-postgresql-downloads . Pilih versi Postgre yang diinginkan, Disini saya menggunakan versi 14.11. Kemudian Klik tombol (icon Panah) untuk download yang ada pada Windows x86-64.\
![PostreSQL][def7]

- Install PostgreSQL yang sudah selesai didownload.\

- Pada PostgreSQL Setup Wizard, pilih next sampai ke pemilihan direktori tempat untuk install PostgreSQL. Kemudian Next.\
![PostreSQL][def8]

- Pada Select Components setup wizard. Cukup ceklis 3 saja yaitu **PostgreSQL Server, pgAdmin 4, dan Comman Line Tools**. Kemudian klik Next.\
![PostreSQL][def9]

- Pilih direktori tempat untuk menyimpan (Storing) database.\
![PostreSQL][def10]

- Setup password untuk PostgreSQL "postgres" user default yang digunakan. **Diharapkan agar mencatat Password yang dibuat, dikarenakan nanti akan dibutuhkan setiap kita ingin mengakses PostgreSQL**. Klik Next.\
![PostreSQL][def11]

- Pilih default port number (5432). **Jika ingin menggunakan Port lain, harap dicatat Portnya untuk nanti agar bisa digunakan**. Klik Next\
![PostreSQL][def12]

- Pada setup wizard Advanced Option. Pilih **Default Locale**. Kemudian Klik Next.\
![PostreSQL][def15]

- Pada setup wizard Pre installation Summary, Klik Next.\
![PostreSQL][def13]

- Pada setup wizard Ready to Install, Klik Next dan tunggu sampai installation selesai.\
![PostreSQL][def14]

- Jika Installation sudah selesai, Klik Finish.

[def7]: img/DownloadPostgreSQL.PNG
[def8]: img/InstallationDirectory.PNG
[def9]: img/SelectComponent.PNG
[def10]: img/DataDirectory.PNG
[def11]: img/Password.PNG
[def12]: img/Port.PNG
[def13]: img/PreInstallation.PNG
[def14]: img/ReadyToInstall.PNG
[def15]: img/AdavanceOption.PNG

> 4. Install IDE

Silahkan pilih Salah satu IDE yang ingin dipakai bisa VsCode, Eclipse, IntelliJ IDE ataupun yang lainnya. Jika memiliki Specification laptop yang tidak terlalu tinggi bisa memilih VSCode. Jika ingin fokus coding di Bahasa Java bisa menggunakan Eclipse.\
Jika ingin menggunakan IDE lengkap dengan Copilot bisa menggunakan IntelliJ IDE tetapi free untuk 30 Day kemudian Berbayar, Atau bisa juga beli licensi nya di Market place Seperti Shopee dengan harga terjangkau untuk 1 tahun.\
Link Download :\
- VScode : https://code.visualstudio.com/Download \
- Eclipse : https://www.eclipse.org/downloads/packages/release/2024-03/r \
- IntelliJ IDE (Jet Brains IDE) : https://www.jetbrains.com/idea/download/?section=windows (Free 30 day)

> 5. Install Postman. Jika menggunakan VSCode sebagai IDE, bisa juga menggunakan Extension Thunder Client.
- Download Postman PC version, buka link : https://www.postman.com/downloads/?utm_source=postman-home.
- Install Postman yang sudah selesai didownload.
![Postman][def16]
[def16]: img/DownloadPostman.PNG
- Jangan lupa untuk register atau buat akun Postman, bisa juga via gmail.

> 6. Install Dbeaver (Optional bisa juga menggunakan Pgadmin yang sudah terinstall dari PostgreSQL). Tetapi untuk Dbever lebih ringan dan simple untuk digunakan. Atau bisa juga menggunakan Tools lainnya seperti Navicat, Sql Workbench dll.
- Download Dbeaver, buka link : https://dbeaver.io/download/.
- Install dbeaver.

